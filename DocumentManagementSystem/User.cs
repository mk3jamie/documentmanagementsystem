﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManagementSystem
{
    /*
     * Doc for User class
     * ---
     * Usage: User x = New User(username, password); 
     *   isPassword is false by default.
     *   User has information about user if username and password matched in database.
     *   isPassword is true if and only if username and password match.
     */
    public class User
    {
        public string username { get; private set; }
        public string password { get; private set; }
        public bool isPassword = false;
        public string userID;
        public string forename;
        public string surname;
        public string role;
        public string status;
        public string regDate;

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
            setUserDetails();
        }

        private void setUserDetails()
        {
            try
            {
                isPassword = sqlWorkBench.isUser(username, password);

                List<string> info = sqlWorkBench.userDetails(username);

                userID = info[0];
                forename = info[1];
                surname = info[2];

                switch (int.Parse(info[3]))
                {
                    case 0:
                        role = "Author";
                        break;
                    case 1:
                        role = "Distributee";
                        break;
                    case 2:
                        role = "Administrator";
                        break;
                }

                switch (int.Parse(info[4]))
                {
                    case 0:
                        status = "Active";
                        break;
                    case 1:
                        status = "Archived";
                        break;
                }

                regDate = info[5];
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }

        }
    }
}

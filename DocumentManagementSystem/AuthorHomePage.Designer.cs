﻿namespace DocumentManagementSystem
{
    partial class AuthorHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorHomePage));
            this.lblAuthorHomePage = new System.Windows.Forms.Label();
            this.btnBackToLog = new System.Windows.Forms.Button();
            this.grbxDraft = new System.Windows.Forms.GroupBox();
            this.lblDraftDocs = new System.Windows.Forms.Label();
            this.ScrlNonActiveDoc = new System.Windows.Forms.HScrollBar();
            this.btnDoc1 = new System.Windows.Forms.Button();
            this.pctbxAddDocument = new System.Windows.Forms.PictureBox();
            this.grbxActive = new System.Windows.Forms.GroupBox();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnDoc2 = new System.Windows.Forms.Button();
            this.btnCreateDocRec = new System.Windows.Forms.Button();
            this.btnEditDoc = new System.Windows.Forms.Button();
            this.btnActivateDoc = new System.Windows.Forms.Button();
            this.btnUploadNewRev = new System.Windows.Forms.Button();
            this.btnReviseDoc = new System.Windows.Forms.Button();
            this.btnViewDoc = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grbxDraft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).BeginInit();
            this.grbxActive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblAuthorHomePage
            // 
            this.lblAuthorHomePage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAuthorHomePage.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuthorHomePage.Location = new System.Drawing.Point(336, 14);
            this.lblAuthorHomePage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAuthorHomePage.Name = "lblAuthorHomePage";
            this.lblAuthorHomePage.Size = new System.Drawing.Size(862, 97);
            this.lblAuthorHomePage.TabIndex = 31;
            this.lblAuthorHomePage.Text = "Author Home Page";
            this.lblAuthorHomePage.Click += new System.EventHandler(this.lblDocumentManagement_Click);
            // 
            // btnBackToLog
            // 
            this.btnBackToLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnBackToLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackToLog.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBackToLog.Location = new System.Drawing.Point(1120, 1169);
            this.btnBackToLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBackToLog.Name = "btnBackToLog";
            this.btnBackToLog.Size = new System.Drawing.Size(267, 108);
            this.btnBackToLog.TabIndex = 28;
            this.btnBackToLog.Text = "Log Out";
            this.btnBackToLog.UseVisualStyleBackColor = false;
            this.btnBackToLog.Click += new System.EventHandler(this.btnBackToUser_Click);
            // 
            // grbxDraft
            // 
            this.grbxDraft.Controls.Add(this.lblDraftDocs);
            this.grbxDraft.Controls.Add(this.ScrlNonActiveDoc);
            this.grbxDraft.Controls.Add(this.btnDoc1);
            this.grbxDraft.Controls.Add(this.pctbxAddDocument);
            this.grbxDraft.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.grbxDraft.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.grbxDraft.Location = new System.Drawing.Point(351, 140);
            this.grbxDraft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbxDraft.Name = "grbxDraft";
            this.grbxDraft.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbxDraft.Size = new System.Drawing.Size(441, 575);
            this.grbxDraft.TabIndex = 42;
            this.grbxDraft.TabStop = false;
            this.grbxDraft.Text = "Draft Documents";
            // 
            // lblDraftDocs
            // 
            this.lblDraftDocs.Location = new System.Drawing.Point(33, 294);
            this.lblDraftDocs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDraftDocs.Name = "lblDraftDocs";
            this.lblDraftDocs.Size = new System.Drawing.Size(372, 222);
            this.lblDraftDocs.TabIndex = 41;
            this.lblDraftDocs.Text = "Draft Documents";
            // 
            // ScrlNonActiveDoc
            // 
            this.ScrlNonActiveDoc.Location = new System.Drawing.Point(33, 529);
            this.ScrlNonActiveDoc.Name = "ScrlNonActiveDoc";
            this.ScrlNonActiveDoc.Size = new System.Drawing.Size(372, 22);
            this.ScrlNonActiveDoc.TabIndex = 40;
            // 
            // btnDoc1
            // 
            this.btnDoc1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc1.BackgroundImage")));
            this.btnDoc1.Location = new System.Drawing.Point(237, 38);
            this.btnDoc1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDoc1.Name = "btnDoc1";
            this.btnDoc1.Size = new System.Drawing.Size(168, 235);
            this.btnDoc1.TabIndex = 32;
            this.btnDoc1.UseVisualStyleBackColor = true;
            // 
            // pctbxAddDocument
            // 
            this.pctbxAddDocument.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctbxAddDocument.BackgroundImage")));
            this.pctbxAddDocument.Location = new System.Drawing.Point(33, 38);
            this.pctbxAddDocument.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctbxAddDocument.Name = "pctbxAddDocument";
            this.pctbxAddDocument.Size = new System.Drawing.Size(168, 235);
            this.pctbxAddDocument.TabIndex = 39;
            this.pctbxAddDocument.TabStop = false;
            // 
            // grbxActive
            // 
            this.grbxActive.Controls.Add(this.hScrollBar1);
            this.grbxActive.Controls.Add(this.button1);
            this.grbxActive.Controls.Add(this.button2);
            this.grbxActive.Controls.Add(this.button3);
            this.grbxActive.Controls.Add(this.btnDoc2);
            this.grbxActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.grbxActive.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.grbxActive.Location = new System.Drawing.Point(351, 726);
            this.grbxActive.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbxActive.Name = "grbxActive";
            this.grbxActive.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbxActive.Size = new System.Drawing.Size(459, 568);
            this.grbxActive.TabIndex = 43;
            this.grbxActive.TabStop = false;
            this.grbxActive.Text = "Active Documents";
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Location = new System.Drawing.Point(33, 529);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(372, 22);
            this.hScrollBar1.TabIndex = 40;
            // 
            // button1
            // 
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.Location = new System.Drawing.Point(237, 38);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 235);
            this.button1.TabIndex = 32;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.Location = new System.Drawing.Point(33, 283);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(168, 235);
            this.button2.TabIndex = 35;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.Location = new System.Drawing.Point(237, 283);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(168, 235);
            this.button3.TabIndex = 36;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnDoc2
            // 
            this.btnDoc2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDoc2.BackgroundImage")));
            this.btnDoc2.Location = new System.Drawing.Point(33, 40);
            this.btnDoc2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDoc2.Name = "btnDoc2";
            this.btnDoc2.Size = new System.Drawing.Size(168, 235);
            this.btnDoc2.TabIndex = 33;
            this.btnDoc2.UseVisualStyleBackColor = true;
            // 
            // btnCreateDocRec
            // 
            this.btnCreateDocRec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCreateDocRec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateDocRec.ForeColor = System.Drawing.SystemColors.Control;
            this.btnCreateDocRec.Location = new System.Drawing.Point(1120, 169);
            this.btnCreateDocRec.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCreateDocRec.Name = "btnCreateDocRec";
            this.btnCreateDocRec.Size = new System.Drawing.Size(267, 114);
            this.btnCreateDocRec.TabIndex = 48;
            this.btnCreateDocRec.Text = "Create Document Record";
            this.btnCreateDocRec.UseVisualStyleBackColor = false;
            this.btnCreateDocRec.Click += new System.EventHandler(this.btnCreateDocRec_Click);
            // 
            // btnEditDoc
            // 
            this.btnEditDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnEditDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditDoc.ForeColor = System.Drawing.SystemColors.Control;
            this.btnEditDoc.Location = new System.Drawing.Point(1120, 938);
            this.btnEditDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEditDoc.Name = "btnEditDoc";
            this.btnEditDoc.Size = new System.Drawing.Size(267, 114);
            this.btnEditDoc.TabIndex = 49;
            this.btnEditDoc.Text = "Edit Document";
            this.btnEditDoc.UseVisualStyleBackColor = false;
            // 
            // btnActivateDoc
            // 
            this.btnActivateDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnActivateDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActivateDoc.ForeColor = System.Drawing.SystemColors.Control;
            this.btnActivateDoc.Location = new System.Drawing.Point(1120, 785);
            this.btnActivateDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnActivateDoc.Name = "btnActivateDoc";
            this.btnActivateDoc.Size = new System.Drawing.Size(267, 114);
            this.btnActivateDoc.TabIndex = 50;
            this.btnActivateDoc.Text = "Activate Document";
            this.btnActivateDoc.UseVisualStyleBackColor = false;
            // 
            // btnUploadNewRev
            // 
            this.btnUploadNewRev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnUploadNewRev.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadNewRev.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUploadNewRev.Location = new System.Drawing.Point(1120, 631);
            this.btnUploadNewRev.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUploadNewRev.Name = "btnUploadNewRev";
            this.btnUploadNewRev.Size = new System.Drawing.Size(267, 114);
            this.btnUploadNewRev.TabIndex = 51;
            this.btnUploadNewRev.Text = "Upload New Revision";
            this.btnUploadNewRev.UseVisualStyleBackColor = false;
            this.btnUploadNewRev.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnReviseDoc
            // 
            this.btnReviseDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnReviseDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReviseDoc.ForeColor = System.Drawing.SystemColors.Control;
            this.btnReviseDoc.Location = new System.Drawing.Point(1120, 477);
            this.btnReviseDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReviseDoc.Name = "btnReviseDoc";
            this.btnReviseDoc.Size = new System.Drawing.Size(267, 114);
            this.btnReviseDoc.TabIndex = 52;
            this.btnReviseDoc.Text = "Revise Document";
            this.btnReviseDoc.UseVisualStyleBackColor = false;
            // 
            // btnViewDoc
            // 
            this.btnViewDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnViewDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewDoc.ForeColor = System.Drawing.SystemColors.Control;
            this.btnViewDoc.Location = new System.Drawing.Point(1120, 323);
            this.btnViewDoc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnViewDoc.Name = "btnViewDoc";
            this.btnViewDoc.Size = new System.Drawing.Size(267, 114);
            this.btnViewDoc.TabIndex = 53;
            this.btnViewDoc.Text = "View Document";
            this.btnViewDoc.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(21, 14);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(306, 298);
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // AuthorHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(1406, 1312);
            this.Controls.Add(this.btnViewDoc);
            this.Controls.Add(this.btnReviseDoc);
            this.Controls.Add(this.btnUploadNewRev);
            this.Controls.Add(this.btnActivateDoc);
            this.Controls.Add(this.btnEditDoc);
            this.Controls.Add(this.btnCreateDocRec);
            this.Controls.Add(this.grbxActive);
            this.Controls.Add(this.grbxDraft);
            this.Controls.Add(this.lblAuthorHomePage);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBackToLog);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "AuthorHomePage";
            this.Text = "AuthorHomePage";
            this.grbxDraft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).EndInit();
            this.grbxActive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctbxAddDocument;
        private System.Windows.Forms.Button btnDoc2;
        private System.Windows.Forms.Button btnDoc1;
        private System.Windows.Forms.Label lblAuthorHomePage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnBackToLog;
        private System.Windows.Forms.GroupBox grbxDraft;
        private System.Windows.Forms.HScrollBar ScrlNonActiveDoc;
        private System.Windows.Forms.GroupBox grbxActive;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnCreateDocRec;
        private System.Windows.Forms.Button btnEditDoc;
        private System.Windows.Forms.Button btnActivateDoc;
        private System.Windows.Forms.Button btnUploadNewRev;
        private System.Windows.Forms.Button btnReviseDoc;
        private System.Windows.Forms.Button btnViewDoc;
        private System.Windows.Forms.Label lblDraftDocs;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
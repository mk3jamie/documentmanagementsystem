﻿namespace DocumentManagementSystem
{
    partial class EditDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstbxDistributees = new System.Windows.Forms.ListBox();
            this.lblCurrentDistributees = new System.Windows.Forms.Label();
            this.btnSubmitNewDocument = new System.Windows.Forms.Button();
            this.btnAddDistributee = new System.Windows.Forms.Button();
            this.txtAddDistributee = new System.Windows.Forms.TextBox();
            this.lblAddDistributee = new System.Windows.Forms.Label();
            this.lblAttach = new System.Windows.Forms.Label();
            this.dtpCreationDate = new System.Windows.Forms.DateTimePicker();
            this.btnCancelCreateNewDocument = new System.Windows.Forms.Button();
            this.lblCreationDate = new System.Windows.Forms.Label();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.lblDocumentStatus = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.lblEditDocumentRecord = new System.Windows.Forms.Label();
            this.txtRevisionNumber = new System.Windows.Forms.TextBox();
            this.txtDocumentTitle = new System.Windows.Forms.TextBox();
            this.lblRevisionNumber = new System.Windows.Forms.Label();
            this.lblDocumentTitle = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ckbxDraft = new System.Windows.Forms.CheckBox();
            this.ckbxActive = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lstbxDistributees
            // 
            this.lstbxDistributees.FormattingEnabled = true;
            this.lstbxDistributees.Items.AddRange(new object[] {
            " "});
            this.lstbxDistributees.Location = new System.Drawing.Point(514, 356);
            this.lstbxDistributees.Name = "lstbxDistributees";
            this.lstbxDistributees.Size = new System.Drawing.Size(238, 108);
            this.lstbxDistributees.TabIndex = 69;
            // 
            // lblCurrentDistributees
            // 
            this.lblCurrentDistributees.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCurrentDistributees.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentDistributees.Location = new System.Drawing.Point(514, 325);
            this.lblCurrentDistributees.Name = "lblCurrentDistributees";
            this.lblCurrentDistributees.Size = new System.Drawing.Size(226, 28);
            this.lblCurrentDistributees.TabIndex = 68;
            this.lblCurrentDistributees.Text = "Current Distributees";
            // 
            // btnSubmitNewDocument
            // 
            this.btnSubmitNewDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnSubmitNewDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmitNewDocument.Location = new System.Drawing.Point(802, 412);
            this.btnSubmitNewDocument.Name = "btnSubmitNewDocument";
            this.btnSubmitNewDocument.Size = new System.Drawing.Size(112, 50);
            this.btnSubmitNewDocument.TabIndex = 67;
            this.btnSubmitNewDocument.Text = "Submit";
            this.btnSubmitNewDocument.UseVisualStyleBackColor = false;
            // 
            // btnAddDistributee
            // 
            this.btnAddDistributee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnAddDistributee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDistributee.Location = new System.Drawing.Point(514, 260);
            this.btnAddDistributee.Name = "btnAddDistributee";
            this.btnAddDistributee.Size = new System.Drawing.Size(200, 51);
            this.btnAddDistributee.TabIndex = 66;
            this.btnAddDistributee.Text = "Add";
            this.btnAddDistributee.UseVisualStyleBackColor = false;
            this.btnAddDistributee.Click += new System.EventHandler(this.btnAddDistributee_Click);
            // 
            // txtAddDistributee
            // 
            this.txtAddDistributee.Location = new System.Drawing.Point(514, 234);
            this.txtAddDistributee.Name = "txtAddDistributee";
            this.txtAddDistributee.Size = new System.Drawing.Size(197, 20);
            this.txtAddDistributee.TabIndex = 65;
            // 
            // lblAddDistributee
            // 
            this.lblAddDistributee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAddDistributee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddDistributee.Location = new System.Drawing.Point(514, 203);
            this.lblAddDistributee.Name = "lblAddDistributee";
            this.lblAddDistributee.Size = new System.Drawing.Size(197, 28);
            this.lblAddDistributee.TabIndex = 64;
            this.lblAddDistributee.Text = "Add Distributee";
            // 
            // lblAttach
            // 
            this.lblAttach.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAttach.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAttach.Location = new System.Drawing.Point(232, 381);
            this.lblAttach.Name = "lblAttach";
            this.lblAttach.Size = new System.Drawing.Size(197, 28);
            this.lblAttach.TabIndex = 63;
            this.lblAttach.Text = "Attach Document";
            // 
            // dtpCreationDate
            // 
            this.dtpCreationDate.Location = new System.Drawing.Point(232, 346);
            this.dtpCreationDate.Name = "dtpCreationDate";
            this.dtpCreationDate.Size = new System.Drawing.Size(200, 20);
            this.dtpCreationDate.TabIndex = 62;
            // 
            // btnCancelCreateNewDocument
            // 
            this.btnCancelCreateNewDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnCancelCreateNewDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelCreateNewDocument.Location = new System.Drawing.Point(802, 474);
            this.btnCancelCreateNewDocument.Name = "btnCancelCreateNewDocument";
            this.btnCancelCreateNewDocument.Size = new System.Drawing.Size(112, 50);
            this.btnCancelCreateNewDocument.TabIndex = 61;
            this.btnCancelCreateNewDocument.Text = "Cancel";
            this.btnCancelCreateNewDocument.UseVisualStyleBackColor = false;
            // 
            // lblCreationDate
            // 
            this.lblCreationDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblCreationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreationDate.Location = new System.Drawing.Point(232, 315);
            this.lblCreationDate.Name = "lblCreationDate";
            this.lblCreationDate.Size = new System.Drawing.Size(197, 28);
            this.lblCreationDate.TabIndex = 60;
            this.lblCreationDate.Text = "Creation Date";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(232, 276);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(197, 20);
            this.txtAuthor.TabIndex = 58;
            // 
            // lblDocumentStatus
            // 
            this.lblDocumentStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentStatus.Location = new System.Drawing.Point(514, 116);
            this.lblDocumentStatus.Name = "lblDocumentStatus";
            this.lblDocumentStatus.Size = new System.Drawing.Size(197, 28);
            this.lblDocumentStatus.TabIndex = 57;
            this.lblDocumentStatus.Text = "Document Status";
            // 
            // lblAuthor
            // 
            this.lblAuthor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuthor.Location = new System.Drawing.Point(232, 245);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(197, 28);
            this.lblAuthor.TabIndex = 56;
            this.lblAuthor.Text = "Author";
            // 
            // lblEditDocumentRecord
            // 
            this.lblEditDocumentRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblEditDocumentRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEditDocumentRecord.Location = new System.Drawing.Point(222, 12);
            this.lblEditDocumentRecord.Name = "lblEditDocumentRecord";
            this.lblEditDocumentRecord.Size = new System.Drawing.Size(607, 62);
            this.lblEditDocumentRecord.TabIndex = 55;
            this.lblEditDocumentRecord.Text = "Edit Document Record";
            // 
            // txtRevisionNumber
            // 
            this.txtRevisionNumber.Location = new System.Drawing.Point(232, 211);
            this.txtRevisionNumber.Name = "txtRevisionNumber";
            this.txtRevisionNumber.Size = new System.Drawing.Size(197, 20);
            this.txtRevisionNumber.TabIndex = 53;
            // 
            // txtDocumentTitle
            // 
            this.txtDocumentTitle.Location = new System.Drawing.Point(232, 147);
            this.txtDocumentTitle.Name = "txtDocumentTitle";
            this.txtDocumentTitle.Size = new System.Drawing.Size(197, 20);
            this.txtDocumentTitle.TabIndex = 52;
            // 
            // lblRevisionNumber
            // 
            this.lblRevisionNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblRevisionNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRevisionNumber.Location = new System.Drawing.Point(232, 180);
            this.lblRevisionNumber.Name = "lblRevisionNumber";
            this.lblRevisionNumber.Size = new System.Drawing.Size(197, 28);
            this.lblRevisionNumber.TabIndex = 51;
            this.lblRevisionNumber.Text = "Revision Number";
            // 
            // lblDocumentTitle
            // 
            this.lblDocumentTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentTitle.Location = new System.Drawing.Point(232, 116);
            this.lblDocumentTitle.Name = "lblDocumentTitle";
            this.lblDocumentTitle.Size = new System.Drawing.Size(197, 28);
            this.lblDocumentTitle.TabIndex = 50;
            this.lblDocumentTitle.Text = "Document Title";
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(232, 412);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(200, 51);
            this.btnBrowse.TabIndex = 49;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 194);
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // ckbxDraft
            // 
            this.ckbxDraft.AutoSize = true;
            this.ckbxDraft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.ckbxDraft.Location = new System.Drawing.Point(619, 165);
            this.ckbxDraft.Name = "ckbxDraft";
            this.ckbxDraft.Size = new System.Drawing.Size(49, 17);
            this.ckbxDraft.TabIndex = 71;
            this.ckbxDraft.Text = "Draft";
            this.ckbxDraft.UseVisualStyleBackColor = false;
            // 
            // ckbxActive
            // 
            this.ckbxActive.AutoSize = true;
            this.ckbxActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.ckbxActive.Location = new System.Drawing.Point(515, 165);
            this.ckbxActive.Name = "ckbxActive";
            this.ckbxActive.Size = new System.Drawing.Size(56, 17);
            this.ckbxActive.TabIndex = 70;
            this.ckbxActive.Text = "Active";
            this.ckbxActive.UseVisualStyleBackColor = false;
            // 
            // EditDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(926, 536);
            this.Controls.Add(this.ckbxDraft);
            this.Controls.Add(this.ckbxActive);
            this.Controls.Add(this.lstbxDistributees);
            this.Controls.Add(this.lblCurrentDistributees);
            this.Controls.Add(this.btnSubmitNewDocument);
            this.Controls.Add(this.btnAddDistributee);
            this.Controls.Add(this.txtAddDistributee);
            this.Controls.Add(this.lblAddDistributee);
            this.Controls.Add(this.lblAttach);
            this.Controls.Add(this.dtpCreationDate);
            this.Controls.Add(this.btnCancelCreateNewDocument);
            this.Controls.Add(this.lblCreationDate);
            this.Controls.Add(this.txtAuthor);
            this.Controls.Add(this.lblDocumentStatus);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.lblEditDocumentRecord);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtRevisionNumber);
            this.Controls.Add(this.txtDocumentTitle);
            this.Controls.Add(this.lblRevisionNumber);
            this.Controls.Add(this.lblDocumentTitle);
            this.Controls.Add(this.btnBrowse);
            this.Name = "EditDocument";
            this.Text = "EditDocument";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstbxDistributees;
        private System.Windows.Forms.Label lblCurrentDistributees;
        private System.Windows.Forms.Button btnSubmitNewDocument;
        private System.Windows.Forms.Button btnAddDistributee;
        private System.Windows.Forms.TextBox txtAddDistributee;
        private System.Windows.Forms.Label lblAddDistributee;
        private System.Windows.Forms.Label lblAttach;
        private System.Windows.Forms.DateTimePicker dtpCreationDate;
        private System.Windows.Forms.Button btnCancelCreateNewDocument;
        private System.Windows.Forms.Label lblCreationDate;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.Label lblDocumentStatus;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label lblEditDocumentRecord;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtRevisionNumber;
        private System.Windows.Forms.TextBox txtDocumentTitle;
        private System.Windows.Forms.Label lblRevisionNumber;
        private System.Windows.Forms.Label lblDocumentTitle;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.CheckBox ckbxDraft;
        private System.Windows.Forms.CheckBox ckbxActive;
    }
}